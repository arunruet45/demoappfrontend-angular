import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  constructor(private router: Router) { }

   public name = 'Arun';
   public myId = 'testId';
   public hasError = true;
   public isSpecial = true;
   public successClass = 'test-success';
   public multipleClasses = {
     'test-success' : this.hasError,
     'test-danger' : this.hasError,
     'test-special' : this.isSpecial
   };
  public eventBinding = '';
  public twoWayDataBinding = '';
  public color = 'red';
  public colors = ['red', 'green', 'yellow'];
  public users = [
    {id: 1, name: 'arun'},
    {id: 2, name: 'barun'},
    {id: 3, name: 'tarun'},
  ];
  @Input() public parentData;
  @Output() public childEvent = new EventEmitter();
  ngOnInit() {
  }
  getUser() {
    return 'name : ' + this.name;
  }

  onClick(event) {
    console.log(event);
    this.eventBinding = 'Event Binding Success';
  }

  logMessage(value) {
    console.log(value);
  }

  fireEvent() {
    this.childEvent.emit('Child to parent');
  }

  // onSelect(user) {
  //   this.router.navigate(['/users', user.id]);
  // }
}
