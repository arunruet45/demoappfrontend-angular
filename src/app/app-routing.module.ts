import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginFormComponent} from './components/login-form/login-form.component';
import {AdminComponent} from './components/admin/admin.component';
import {SignUpComponent} from './components/sign-up/sign-up.component';
import {AllQuestionComponent} from './components/all-question/all-question.component';
import {OwnQuestionsComponent} from './components/own-questions/own-questions.component';
import {QuestionAddComponent} from './components/question-add/question-add.component';
import {CategoryAddComponent} from './components/category-add/category-add.component';
import {UserComponent} from './components/user/user.component';
import { AdminRouteGuardService } from './services/admin-route-guard.service';

const routes: Routes = [
  {path: '', redirectTo: '/loginForm', pathMatch: 'full'},
  {path: 'loginForm', component: LoginFormComponent},
  {path: 'signupForm', component: SignUpComponent},
  {path: 'admin', component: AdminComponent,
    children: [
      {path: 'questions/all', component: AllQuestionComponent},
      {path: 'questions/own', component: OwnQuestionsComponent},
      {path: 'questions/add', component: QuestionAddComponent},
      {path: 'category/all', component: CategoryAddComponent},
      {path: 'category/add', component: OwnQuestionsComponent},
    ]},
  {path: 'users', component: UserComponent,
    children: [
      {path: 'questions/all', component: AllQuestionComponent},
      {path: 'questions/own', component: OwnQuestionsComponent},
      {path: 'questions/add', component: QuestionAddComponent},
    ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [LoginFormComponent, AdminComponent, SignUpComponent, AllQuestionComponent];
