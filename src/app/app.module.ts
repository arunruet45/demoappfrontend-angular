import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule, routingComponents} from './app-routing.module';
import { AppComponent } from './app.component';
import {UserDataService} from './services/user-data.service';
import {UserLoginService} from './services/user-login.service';
import {HttpClientModule} from '@angular/common/http';

import {FormsModule} from '@angular/forms';
import {TestComponent} from './test/test.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { AdminComponent } from './components/admin/admin.component';
import { CategoryAddComponent } from './components/category-add/category-add.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { AllQuestionComponent } from './components/all-question/all-question.component';
import {CookieService} from 'ngx-cookie-service';
import { OwnQuestionsComponent } from './components/own-questions/own-questions.component';
import { QuestionAddComponent } from './components/question-add/question-add.component';
import { AnswerComponent } from './components/answer/answer.component';
import { UserComponent } from './components/user/user.component';


@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    TestComponent,
    AdminComponent,
    CategoryAddComponent,
    NavbarComponent,
    SidebarComponent,
    AllQuestionComponent,
    SignUpComponent,
    OwnQuestionsComponent,
    QuestionAddComponent,
    AnswerComponent,
    UserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [UserDataService, UserLoginService, CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
