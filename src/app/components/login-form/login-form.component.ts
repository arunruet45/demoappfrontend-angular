import { Component, OnInit } from '@angular/core';
import {HttpHeaders} from '@angular/common/http';
import {UserLoginService} from '../../services/user-login.service';
import {CookieService} from 'ngx-cookie-service';
import {UserDataService} from '../../services/user-data.service';
import {Router} from '@angular/router';
import {SharedDataService} from '../../services/shared-data.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  emailError: string;
  passwordError: string;

  constructor(private userLoginService: UserLoginService, private userDataService: UserDataService,
     private router: Router, private sharedDataService: SharedDataService) { }
  ngOnInit() {
    if (localStorage.getItem('sessionId') != null) {
      this.userDataService.getUserInfo()
        .subscribe(
          (userData: any) => {
              if (userData.body.type === 'admin') {
                this.router.navigate(['/admin']);
              } else {
                this.router.navigate(['/users']);
              }
          });
    }
  }

  userLoginInfo =  new FormGroup({
    'email': new FormControl('', Validators.required),
    'password': new FormControl('', [Validators.required, Validators.minLength(4)])
  })

  get email() {
    return this.userLoginInfo.get('email');
  }

  get password() {
    return this.userLoginInfo.get('password');
  }

  onSubmit() {
    this.userLoginService.login(this.userLoginInfo.value)
      .subscribe(
        (loginResponseData: any) => {
           localStorage.setItem('sessionId', loginResponseData.body.sessionId);
           console.log(loginResponseData);
           if (loginResponseData.body.sessionId) {
              this.userDataService.getUserInfo()
                .subscribe(
                  (userData: any) => {
                    console.log(userData);
                    this.sharedDataService.changeData(userData.body);
                    this.sharedDataService.currentData
                    .subscribe(
                      (data) => {
                        if ( data['type'] === 'admin') {
                          this.router.navigate(['/admin']);
                        } else {
                          this.router.navigate(['/users']);
                        }
                      }
                    )
                  }
                );
            } else {
             this.router.navigate(['/loginForm']);
           }
        },
        (error: any) => {
          console.log(error);
          if (error.error.message === 'Password Not Found') {
            this.passwordError = 'Password Not Found';
            console.log('pass');
          }
          if (error.error.message === 'Email Not Found') {
            this.emailError = 'Email Not Found';
            console.log('email');
          }
        }
      );
  }
}
