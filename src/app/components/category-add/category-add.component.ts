import { Component, OnInit } from '@angular/core';
import {Category} from '../../services/category';
import {CategoryService} from '../../services/category.service';

@Component({
  selector: 'app-category-add',
  templateUrl: './category-add.component.html',
  styleUrls: ['./category-add.component.css']
})
export class CategoryAddComponent implements OnInit {
  updatedCategory = new Category();
  category = new Category();
  categories: any[];
  previousUpdateId: number;
  categoryExist: string;
  categoryAdded: string;
  categoryDeleted: string;
  constructor(private categoryService: CategoryService) {
    this.categories = [];
  }

  ngOnInit() {
    this.categoryService.getAllCategory()
      .subscribe(
        (data: any) => {
          this.categories = data.body;
          console.log(this.categories);
        }
      );
  }

  onSubmit() {
    this.categoryService.addCategory(this.category)
      .subscribe(
        (data: any) => {
          console.log('done');
          this.categoryAdded = 'Category Added';
        },
        (error: any) => {
          if (error.error.status === 409) {
            console.log('categoroy');
            this.categoryExist = 'Category Exist.';
          }
        }
      );
  }

  updateCategory(id: number) {
    if (this.previousUpdateId === id) {
      document.getElementById(String(id)).style.display = 'none';
      this.previousUpdateId = 0;
      return;
    }
    for (const category of this.categories) {
      if (category.id === id) {
        document.getElementById(String(id)).style.display = 'block';
      } else {
        document.getElementById(String(category.id)).style.display = 'none';
      }
    }
    this.previousUpdateId = id;
  }

  onUpdateSubmit(id: number) {
    this.categoryService.updateCategory(this.updatedCategory, id)
      .subscribe(
        (data: any) => {
          if (data.body.status === 200) {
            console.log('updated');
          }
        }
      );
  }

  deleteCategory(id: number) {
    this.categoryService.deleteCategory(id)
      .subscribe(
        (data: any) => {
          console.log('successfully deleted');
          this.categoryDeleted = 'Category Deleted';
          this.categories = this.categories.filter(value => value.id !== id);
        }
      );
  }

}
