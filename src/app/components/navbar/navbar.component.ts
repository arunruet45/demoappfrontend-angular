import { Component, OnInit } from '@angular/core';
import {SharedDataService} from '../../services/shared-data.service';
import {UserDataService} from '../../services/user-data.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  userEmail: string;
  userInfo: any;
  constructor(private sharedDataService: SharedDataService, private userDataService: UserDataService, private router: Router) {
    this.userInfo = {};
  }

  ngOnInit() {
    this.sharedDataService.currentData
      .subscribe(
        (data) => {
          this.userEmail = data['email'];
        }
      )
  }

  onLogout() {
    this.userDataService.logout()
      .subscribe(
        (logoutResponseData: any) => {
          console.log(logoutResponseData);
          if (logoutResponseData.body.status === 200) {
            this.router.navigate(['/loginForm']);
            localStorage.removeItem('sessionId');
          } else {
            this.router.navigate(['/loginForm']);
          }
        }
      );
  }
}
