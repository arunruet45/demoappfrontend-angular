import { Component, OnInit } from '@angular/core';
import {UserSignupService} from '../../services/user-signup.service';
import {Router} from '@angular/router';
import {UserDataService} from '../../services/user-data.service';
import {SharedDataService} from '../../services/shared-data.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  emailExist: string;

  constructor(private userSignupService: UserSignupService, private router: Router, private userDataService: UserDataService,
              private sharedDataService: SharedDataService) { }

  ngOnInit() {
    if (localStorage.getItem('sessionId') != null) {
    this.userDataService.getUserInfo()
      .subscribe(
        (userData: any) => {
          if (userData.body.type === 'admin') {
            this.router.navigate(['/admin']);
          } else {
            this.router.navigate(['/users']);
          }
        });
    }
  }

  userSignupInfo =  new FormGroup({
    'email': new FormControl('', Validators.required),
    'password': new FormControl('', [Validators.required, Validators.minLength(4)])
  })

  get email() {
    return this.userSignupInfo.get('email');
  }

  get password() {
    return this.userSignupInfo.get('password');
  }

  onSubmit() {
    this.userSignupService.signup(this.userSignupInfo.value)
      .subscribe (
        (signupResponseData: any) => {
          if (signupResponseData.body.status === 200) {
          localStorage.setItem('sessionId', signupResponseData.body.sessionId);
          console.log(signupResponseData);
          if (signupResponseData.body.sessionId) {

            this.userDataService.getUserInfo()
              .subscribe(
                (userData: any) => {
                  console.log(userData);
                  this.sharedDataService.changeData(userData.body);
                  this.sharedDataService.currentData
                  .subscribe(
                    (data) => {
                      if ( data['type'] === 'admin') {
                        this.router.navigate(['/admin']);
                      } else {
                        this.router.navigate(['/users']);
  
                      }
                    }
                  )
                    
                  
                }
              );
          } else {
            this.router.navigate(['/loginForm']);
          }
        }
          if (signupResponseData.body.status === 226) {
            console.log('Email Error from Angular');
            this.emailExist = 'Email Already Exist';
          }
          },
      );
  }
}
