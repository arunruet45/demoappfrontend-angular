import { Component, OnInit } from '@angular/core';
import {QuestionService} from '../../services/question.service';
import {AnswerService} from '../../services/answer.service';
import {Answer} from '../../services/answer';

@Component({
  selector: 'app-all-question',
  templateUrl: './all-question.component.html',
  styleUrls: ['./all-question.component.css']
})
export class AllQuestionComponent implements OnInit {
  allQuestions: any[];
  answers: any[];
  answer = new Answer();
  private id: number;
  previousDescriptionId = 0;
  previousAnswerId = 0;
  answerNotFound: string;
  constructor(private allQuestionService: QuestionService, private answerService: AnswerService) {
    this.allQuestions = [];
    this.answers = [];
    this.answerNotFound = null;
  }

  ngOnInit() {
    this.allQuestionService.allQuestions()
      .subscribe(
        (allQuestions: any) => {
          console.log(allQuestions);
          this.allQuestions = allQuestions.body;
        }
      );
    for (const question of this.allQuestions) {
      document.getElementById('des' + String(question.id)).style.display = 'none';
    }

  }

  questionDescription(id: number) {
    if (this.previousDescriptionId === id) {
      document.getElementById('des' + String(id)).style.display = 'none';
      this.previousDescriptionId = 0;
      return;
    } else {
      for (const question of this.allQuestions) {
        if (question.id === id) {
          document.getElementById('des' + String(id)).style.display = 'block';
        } else {
          document.getElementById('des' + String(question.id)).style.display = 'none';
        }
      }
      this.previousDescriptionId = id;
    }
  }

  onAnswer(id: number) {
    this.answerService.getanswers(id)
      .subscribe(
        (data: any) => {
          this.answers = data.body;
          console.log(this.answers);
        },
        (error: any) => {
          if (error.status === 404) {
            this.answerNotFound = 'Answer Not found';
          }
        }
      );

    if (this.previousDescriptionId === id) {
      document.getElementById('des' + String(id)).style.display = 'none';
      this.previousDescriptionId = 0;
      return;
    } else {
      for (const question of this.allQuestions) {
        if (question.id === id) {
          document.getElementById('des' + String(id)).style.display = 'block';
        } else {
          document.getElementById('des' + String(question.id)).style.display = 'none';
        }
      }
      this.previousDescriptionId = id;
    }
  }
  onAnswerSubmit() {

  }
  deleteQuestion(id: number) {
    this.allQuestionService.deleteQuestion(id)
      .subscribe(
        (data: any) => {
          if (data.body.status === 200) {
            console.log('successfully deleted');
            this.allQuestions = this.allQuestions.filter(value => value.id !== id);
          }
        }
      );
  }
}
