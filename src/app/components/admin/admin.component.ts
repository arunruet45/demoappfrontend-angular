import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SharedDataService} from '../../services/shared-data.service';
import {UserDataService} from '../../services/user-data.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  userInfo: any;
  isShow: boolean;
  // tslint:disable-next-line:max-line-length
  constructor(private route: ActivatedRoute, private router: Router, private sharedDataService: SharedDataService, private userDataService: UserDataService) {
    this.userInfo = {};
  }

  ngOnInit() {
    this.userDataService.getUserInfo()
      .subscribe(
        (response: any) => {
          // console.log(response);
        },
        // tslint:disable-next-line:no-shadowed-variable
        (error: any) => {
          if (error.status === 401) {
            this.router.navigate(['/loginForm']);
          }
        }
      );
  }
}
