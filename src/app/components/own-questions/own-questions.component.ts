import { Component, OnInit } from '@angular/core';
import {QuestionService} from '../../services/question.service';
import {UserDataService} from '../../services/user-data.service';
import {CategoryService} from '../../services/category.service';
import {Question} from '../../services/question';
import {AnswerService} from '../../services/answer.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-own-questions',
  templateUrl: './own-questions.component.html',
  styleUrls: ['./own-questions.component.css']
})
export class OwnQuestionsComponent implements OnInit {
  updatedQuestion = new Question();
  ownQuestions: any[];
  categories: any[];
  answers: any[];
  id: number;
  previousUpdateId: number;
  previousDescriptionId: number;
  previousAnswerId: number;
  selectedQuestionDescriptionId: number;
  selectedQuestionUpdateId: number;
  selectedQuestionAnswerId: number;
  isEnabled: boolean;
  // isShow: boolean;
  categoryId: number;
  isDescriptionEnable: boolean;
  isUpdateEnable: boolean; 
  isAnswerEnable: boolean;
  constructor(private questionService: QuestionService, private userDataService: UserDataService,
              private categoryService: CategoryService, private answerService: AnswerService) {
    this.ownQuestions = [];
    this.categories = [];
    this.answers = [];
    this.selectedQuestionDescriptionId = 0;
    this.selectedQuestionUpdateId = 0;
    this.selectedQuestionAnswerId = 0;
    this.previousDescriptionId = 0;
    this.previousUpdateId = 0;
    this.previousAnswerId = 0;
    this.isEnabled = true;
    this.isDescriptionEnable = false;
    this.isUpdateEnable = false;
    this.isAnswerEnable = false;
  }

  ngOnInit() {
    // this.isShow = true;
    if (localStorage.getItem('sessionId') != null) {
      this.userDataService.getUserInfo()
        .subscribe(
          (userData: any) => {
            this.id = userData.body.id;
            console.log('user ID: ', this.id);
            this.questionService.ownQuestions(this.id)
              .subscribe(
                (ownQuestions) => {
                  console.log(ownQuestions);
                  this.ownQuestions = ownQuestions.body;
                  this.categoryService.getAllCategory()
                    .subscribe(
                      (categories: any) => {
                        this.categories = categories.body;
                      }
                    );
                }
              );
          }
          );
      for (const question of this.ownQuestions) {
        document.getElementById('up' + String(question.id)).style.display = 'none';
      }
      // for (const question of this.ownQuestions) {
      //   document.getElementById('des' + String(question.id)).style.display = 'none';
      // }
      // for (const question of this.ownQuestions) {
      //   document.getElementById(des+question.id).style.display = 'none';
      // }
    }

  }

  questionDescription(id: number) {
    this.ownQuestions.forEach(question => {
      this.selectedQuestionDescriptionId = 0;
      this.selectedQuestionUpdateId = 0;
      this.selectedQuestionAnswerId = 0;
    })
    if(this.previousDescriptionId !== id) {
      this.selectedQuestionDescriptionId = id;
      if ((this.previousUpdateId === id) && this.isUpdateEnable) {
        this.selectedQuestionUpdateId = id;
      }
      this.isDescriptionEnable = true;
    } else {
      if (this.isDescriptionEnable) {
        this.isDescriptionEnable = false;
        this.selectedQuestionDescriptionId = 0;
        this.isAnswerEnable = false;
      } else {
        this.isDescriptionEnable = true;
        this.selectedQuestionDescriptionId = id;
      }
      if ((this.previousUpdateId === id) && this.isUpdateEnable) {
        this.selectedQuestionUpdateId = id;
      }
       
    }
    this.previousDescriptionId = id;
  }

  answer(id: number) {
    this.answerService.getanswers(id)
      .subscribe(
        (data: any) => {
          this.answers = data.body;
          console.log(this.answers);
          this.answers.forEach(answer => {
            this.selectedQuestionDescriptionId = 0;
            this.selectedQuestionUpdateId = 0;
            this.selectedQuestionAnswerId = 0;
          })
          if(this.previousAnswerId !== id) {
            this.selectedQuestionAnswerId = id;
            if ((this.previousUpdateId === id) && this.isUpdateEnable) {
              this.selectedQuestionUpdateId = id;
            }
            if ((this.previousDescriptionId === id) && this.isDescriptionEnable) {
              this.selectedQuestionDescriptionId = id;
            }
            this.isAnswerEnable = true;
          } else {
            if (this.isAnswerEnable) {
              this.isAnswerEnable = false;
              this.selectedQuestionAnswerId = 0;
            } else {
              this.isAnswerEnable = true;
              this.selectedQuestionAnswerId = id;
            }
            if ((this.previousUpdateId === id) && this.isUpdateEnable) {
              this.selectedQuestionUpdateId = id;
            }
            if ((this.previousDescriptionId === id) && this.isDescriptionEnable) {
              this.selectedQuestionDescriptionId = id;
            }
             
          }
          this.previousAnswerId = id;
        }
      );
  }

  deleteQuestion(id: number) {
    this.questionService.deleteQuestion(id)
      .subscribe(
        (data: any) => {
          console.log('successfully deleted');
          this.ownQuestions = this.ownQuestions.filter(value => value.id !== id);
        }
      );
  }
  
  updateQuestionForm = new FormGroup({
    title: new FormControl('', Validators.required),
    categoryId: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required)
  })

  updateQuestion(id: number, title: string, categoryId: number, description: string) {
    this.ownQuestions.forEach(question => {
      this.selectedQuestionUpdateId = 0;
      this.selectedQuestionDescriptionId = 0;
    })
    if(this.previousUpdateId !== id) {
      this.selectedQuestionUpdateId = id;
      if ((this.previousDescriptionId === id) && this.isDescriptionEnable) {
        this.selectedQuestionDescriptionId = id;
      }
      this.isUpdateEnable = true;
    } else {
      if (this.isUpdateEnable) {
        this.isUpdateEnable = false;
        this.selectedQuestionUpdateId = 0;
      } else {
        this.isUpdateEnable = true;
        this.selectedQuestionUpdateId = id;
      }
      if ( (this.previousDescriptionId === id) && this.isDescriptionEnable) {
        this.selectedQuestionDescriptionId = id;
      }
      
    }
    this.previousUpdateId = id;
    
    this.updateQuestionForm.patchValue({
      title: title,
      categoryId: categoryId,
      description: description
    })
  }

  onUpdateSubmit(id: number) {
    console.log(this.updateQuestionForm.value);
    this.questionService.updateQuestion(this.updateQuestionForm.value, id)
      .subscribe(
        (data: any) => {
          if (data.body.status === 200) {
            console.log('updated');
            this.ngOnInit();
          }
        }
      );
  }

  // questionDes(id: number) {
  //   this.selectedQuestionDescriptionId = id;
  // }

}
