import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OwnQuestionsComponent } from './own-questions.component';

describe('OwnQuestionsComponent', () => {
  let component: OwnQuestionsComponent;
  let fixture: ComponentFixture<OwnQuestionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OwnQuestionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnQuestionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
