import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { SharedDataService } from '../../services/shared-data.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  userType: string;
   _isUserTypeIsAdmin: boolean;
  constructor(private router: Router, private sharedDataService: SharedDataService) { }

  ngOnInit() {
    this._isUserTypeIsAdmin = true;
    this.sharedDataService.currentData
      .subscribe (
        (data) => {
          this.userType = data['type'];
        } 
      )
  }

  get isUserTypeIsAdmin(): boolean {
    if (this.userType !== 'admin')
      return false;
  }

}
