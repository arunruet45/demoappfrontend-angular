import { Component, OnInit } from '@angular/core';
import {QuestionService} from '../../services/question.service';
import {Question} from '../../services/question';
import {CategoryService} from '../../services/category.service';
import {ActivatedRoute, Router} from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-question-add',
  templateUrl: './question-add.component.html',
  styleUrls: ['./question-add.component.css']
})
export class QuestionAddComponent implements OnInit {
  categories: any[];
  categoryId: string;s
  response: string;
  constructor(private questionService: QuestionService, private categoryService: CategoryService, private router: Router, private route: ActivatedRoute) {
    this.categories = [];
  }

  ngOnInit() {
    this.categoryService.getAllCategory()
      .subscribe(
        (categories: any) => {
          this.categories = categories.body;
        }
      );
  }
    onSubmit() {
      console.log(this.question.value);
      this.questionService.addQuestion(this.question.value)
        .subscribe(
          (response: any) => {
            if (response.body.status === 200) {
              this.response = 'Question Added';
              this.router.navigate(['/admin/questions/add']);
            }
          }
        );
    }

    question = new FormGroup({
      title: new FormControl('', Validators.required),
      categoryId: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required)
    })

    get title() {
      return this.question.get('title');
    }
    
    get category() {
      return this.question.get('category');
    }

    get description() {
      return this.question.get('description');
    }
}
