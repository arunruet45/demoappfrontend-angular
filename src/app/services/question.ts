export class Question {
  public title: string;
  public categoryId: string;
  public description: string;
}
