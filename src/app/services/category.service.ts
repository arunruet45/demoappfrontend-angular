import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Category} from './category';
import {Question} from './question';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  allCategoryUrl = 'http://localhost:8080/api/v1/users/categories/findAll';
  addCategoryUrl = 'http://localhost:8080/api/v1/admin/categories/add';
  updateCategoryUrl = 'http://localhost:8080/api/v1/admin/categories/update/';
  deleteCategoryUrl = 'http://localhost:8080/api/v1/admin/categories/delete/';

  constructor(private http: HttpClient) { }

  getAllCategory(): Observable<any> {
    const abc = localStorage.getItem('sessionId');
    const headers: HttpHeaders = new HttpHeaders({
      sessionId : abc
    });
    return this.http.get(this.allCategoryUrl, {observe: 'response', headers});
  }

  addCategory(category: Category): Observable<any> {
    const abc = localStorage.getItem('sessionId');
    const headers: HttpHeaders = new HttpHeaders({
      sessionId : abc
    });
    return this.http.post(this.addCategoryUrl, category, {observe: 'response', headers});
  }

  updateCategory(category: Category, categoryId: number) {

    const abc = localStorage.getItem('sessionId');
    const headers: HttpHeaders = new HttpHeaders({
      sessionId : abc
    });
    // const params: HttpParams = new HttpParams();
    // params.append('categoryId', String(categoryId));
    return this.http.patch(this.updateCategoryUrl + categoryId, category,
      {headers, observe: 'response'});
  }

  deleteCategory(id: number) {
    const abc = localStorage.getItem('sessionId');
    const headers: HttpHeaders = new HttpHeaders({
      sessionId : abc
    });
    return this.http.delete(this.deleteCategoryUrl + id, {observe: 'response', headers});
  }
}
