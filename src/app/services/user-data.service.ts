import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CookieService} from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {

  loginUserDataUrl = 'http://localhost:8080/api/v1/users/info';
  logoutUrl = 'http://localhost:8080/api/v1/users/logOut';

  constructor(private http: HttpClient) { }

  getUserInfo(): Observable<any> {
    const abc = localStorage.getItem('sessionId');
    const headers: HttpHeaders = new HttpHeaders({
      sessionId : abc
    });
    return this.http.get(this.loginUserDataUrl, {observe: 'response', headers});
  }
  logout(): Observable<any> {
    const abc = localStorage.getItem('sessionId');
    const headers: HttpHeaders = new HttpHeaders({
      sessionId : abc
    });
    return this.http.post(this.logoutUrl, {}, {observe: 'response', headers});
  }
}
