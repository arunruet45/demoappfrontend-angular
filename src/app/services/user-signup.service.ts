import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserSignupService {

  url = 'http://localhost:8080/api/v1/users/signUp';

  constructor(private http: HttpClient) { }

  signup(user: any) {
    return this.http.post<any>(this.url, user, {observe: 'response'});
  }
}
