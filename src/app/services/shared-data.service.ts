import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedDataService {
  userInfo: any;
  // userInfo: any;
  constructor() {
    this.userInfo = {};
  }
  private dataSource = new BehaviorSubject<Object>({});
  currentData = this.dataSource.asObservable();

  changeData(userData: any) {
    this.dataSource.next(userData);
  }
}
