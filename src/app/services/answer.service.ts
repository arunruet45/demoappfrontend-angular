import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AnswerService {

  getAnswersUrl = 'http://localhost:8080/api/v1/users/questions/';
  constructor(private http: HttpClient) { }

  getanswers(id: number): Observable<any> {
    const abc = localStorage.getItem('sessionId');
    const headers: HttpHeaders = new HttpHeaders({
      sessionId : abc
    });
    return this.http.get(this.getAnswersUrl + id + '/answers/findAll', {observe: 'response', headers});
  }
}
