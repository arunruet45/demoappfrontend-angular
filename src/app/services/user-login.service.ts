import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse, HttpHeaders} from '@angular/common/http';
import {observable, Observable} from 'rxjs';
import {CookieService} from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class UserLoginService {


  url = 'http://localhost:8080/api/v1/users/login';

  constructor(private http: HttpClient, private cookieService: CookieService) { }

  login(user: any) {
    return this.http.post<any>(this.url, user, {observe: 'response'});
  }
}
