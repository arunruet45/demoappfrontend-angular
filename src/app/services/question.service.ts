import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Question} from './question';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  allQuestionsUrl = 'http://localhost:8080/api/v1/users/questions/findAll';
  deleteQuestionUrl = 'http://localhost:8080/api/v1/users/questions/delete/';
  ownQuestionsUrl = 'http://localhost:8080/api/v1/users/';
  updateQuestionUrl = 'http://localhost:8080/api/v1/users/questions/update/';
  addQuestionUrl = 'http://localhost:8080/api/v1/users/questions/add';

  constructor(private http: HttpClient) { }

  allQuestions(): Observable<any> {
    const abc = localStorage.getItem('sessionId');
    const headers: HttpHeaders = new HttpHeaders({
      sessionId : abc
    });
    return this.http.get(this.allQuestionsUrl, {observe: 'response', headers});
  }

  ownQuestions(id: number): Observable<any> {
    const abc = localStorage.getItem('sessionId');
    const headers: HttpHeaders = new HttpHeaders({
      sessionId : abc
    });
    return this.http.get(this.ownQuestionsUrl + id + '/questions/findAll', {observe: 'response', headers});
  }

  addQuestion(question: any): Observable<any> {
    const abc = localStorage.getItem('sessionId');
    const headers: HttpHeaders = new HttpHeaders({
      sessionId : abc
    });
    return this.http.post<Question>(this.addQuestionUrl, question,
      {headers, observe: 'response', params: {categoryId: question.categoryId}});
  }

  deleteQuestion(id: number) {
    const abc = localStorage.getItem('sessionId');
    const headers: HttpHeaders = new HttpHeaders({
      sessionId : abc
    });
    return this.http.delete(this.deleteQuestionUrl + id, {observe: 'response', headers});
  }

  updateQuestion(question: any, questionId: number) {

    const abc = localStorage.getItem('sessionId');
    const headers: HttpHeaders = new HttpHeaders({
      sessionId : abc
    });
    // const params: HttpParams = new HttpParams();
    // params.append('categoryId', String(categoryId));
    return this.http.patch(this.updateQuestionUrl + questionId, question,
      {headers, observe: 'response', params: {categoryId: question.categoryId}});
  }
}
